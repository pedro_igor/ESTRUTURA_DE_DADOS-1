#ifndef PONTO_H
#define PONTO_H

typedef struct ponto Ponto;

Ponto* criaPt(float x, float y);

void liberaPt(Ponto* p);

void acessaPt(Ponto* p, float *x, float *y);

void atribuiPt(Ponto* p, float x, float y);

#endif // !PONTO_H
