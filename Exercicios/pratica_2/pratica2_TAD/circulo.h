#ifndef CIRCULO_H
#define CIRCULO_H

#include "ponto.h"

#define PI 3.141592

typedef struct circulo Circulo;

Circulo* criaCir(Ponto* ponto, float r);

void liberaCir(Circulo* cir);

float areaCir(Circulo* c);

#endif // !CIRCULO_H
