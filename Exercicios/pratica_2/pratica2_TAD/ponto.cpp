//ponto.c
#include <stdio.h>
#include <stdlib.h>
#include "ponto.h"

struct ponto
{
	float x;
	float y;
};

Ponto* criaPt(float x, float y)
{
	Ponto* pt = (Ponto*)malloc(sizeof(Ponto));

	if (pt == NULL)
	{
		printf("ERRO: Memoria insufiente.\n");
		exit(1);
	}

	pt->x = x;
	pt->y = y;

	return pt;
}

void liberaPt(Ponto* p)
{
	free(p);
}

void acessaPt(Ponto* p, float *x, float *y)
{
	*x = p->x;
	*y = p->y;
}

void atribuiPt(Ponto* p, float x, float y)
{
	p->x = x;
	p->y = y;
}