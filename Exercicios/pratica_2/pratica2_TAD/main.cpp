#include <stdio.h>
#include "ponto.h"
#include "circulo.h"

void questao1();
void questao2();
void menu();

int main()
{
	menu();
	return 0;
}

void questao1()
{
	Ponto* pt;
	float x, y;

	scanf("%f %f", &x, &y);

	pt = criaPt(x, y);

	atribuiPt(pt, x, y);

	acessaPt(pt, &x, &y);

	printf("Os valores de ponto sao:\nx: %f \ny: %f\n", x, y);

	liberaPt(pt);
}

void questao2()
{
	Circulo* cir;

	float x, y, r;

	scanf("%f %f", &x, &y);

	Ponto* pt = criaPt(x, y);

	scanf("%f", &r);

	cir = criaCir(pt, r);

	float a = areaCir(cir);

	printf("A area do circulo eh: %f\n", a);

	liberaPt(pt);
	liberaCir(cir);
}

void menu()
{
	int op;//opcao

	scanf("%d", &op);

	do
	{
		switch (op)
		{
		case 1:
			printf("Questao 1\n");

			questao1();

			break;

		case 2:
			printf("\nQuestao 2\n");

			questao2();

			break;
		default:
			break;
		}

		scanf("%d", &op);
	} while (op != 0);
}