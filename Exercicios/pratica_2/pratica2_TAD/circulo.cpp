#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "circulo.h"
#include "ponto.h"

struct circulo
{
	float raio;
	Ponto* centro;
};

Circulo* criaCir(Ponto *ponto, float r)//raio
{
	Circulo* cir = (Circulo*)malloc(sizeof(Circulo));

	if (cir == NULL)
	{
		printf("ERRO: Memoria insufiente\n");
		exit(1);
	}

	cir->centro = ponto;
	cir->raio = r;

	return cir;
}

void liberaCir(Circulo* cir)
{
	free(cir);
}

float areaCir(Circulo* cir)
{
	float a = PI * pow(cir->raio, 2);

	return a; //area do curculo;
}