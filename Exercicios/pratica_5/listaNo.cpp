#include "listaNo.hpp"

struct listano
{
    int info;
    ListaNo* prox;
};

int get_info(ListaNo* no)
{
	return no->info;
}

ListaNo* get_prox(ListaNo* no)
{
	return no->prox;
}

void set_info(ListaNo* no, int info)
{
	no->info = info;
}

void set_prox(ListaNo* no, ListaNo* prox)
{
	no->prox = prox;
}

int get_tam(void);
{
	return sizeof(ListaNo);
}