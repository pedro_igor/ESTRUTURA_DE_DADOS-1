#include "lista.hpp"
#include "listaNo.hpp"

struct lista
{
    ListaNo* prim;
	ListaNo* ult;
};

Lista* lst_cria(void)
{
    Lista* l = (Lista*) malloc(sizeof(Lista));
    l->prim = NULL;

    return l;
}

void lst_libera(Lista* l)
{
    ListaNo* p = l -> prim;

	while (p != NULL)
	{
		ListaNo* t = get_prox(p);
		free(p);
		p = t;
	}

	free(l);
}

void lst_insere(Lista* l, int v)
{
	ListaNo* novo = (ListaNo*)malloc(get_tam());
	set_info(novo, v);
	set_prox(novo, l->prim);
	l->prim = novo;
}

int lst_pertence(ListaNo* no, int v)
{
	if (no == NULL)
	{
		return 0;
	}
	else
	{
		if (get_info(no) != v)
		{
			return lst_pertence(get_prox(no), v);
		}
		else
		{
			return 1;
		}
	}

}

int lista_pertence(Lista* l, int v)
{
	return lst_pertence(l->prim, v);
}