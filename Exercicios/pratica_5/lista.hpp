#ifndef LISTA_HPP
#define LISTA_HPP

#include <iostream>
#include <cstdlib>

using namespace std;

typedef struct lista Lista;

Lista* lst_cria(void);
void lst_libera(Lista* l);

void lst_insere (Lista* l, int v);
void lst_insere_ordernado(Lista* l , int v);
void lsd_retira(Lista* l, int v);

int lst_vazia(Lista* l);
int lst_pertence(Lista* l, int v);
int lista_pertence(Lista* l, int v);
void lst_imprime(Lista* l);

#endif //!LISTA_HPP