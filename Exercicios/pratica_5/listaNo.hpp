#ifndef LISTANO_HPP
#define LISTANO_HPP

#include <iostream>
using namespace std;

typedef struct listano ListaNo;

int get_info(ListaNo* no);
void set_info(ListaNo* no, int info);
ListaNo* get_prox(ListaNo* no);
void set_prox(ListaNo* no, ListaNo* prox);
int get_tam();
#endif //!LISTANO_HPP