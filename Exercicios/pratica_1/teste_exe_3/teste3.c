#include<stdio.h>
#include<stdlib.h>
#include<limits.h>

int** aloca_matriz(int linha, int coluna);
int** libera_matriz(int linha, int coluna, int** matriz);
void menor_maior(int linha, int coluna, int** matriz, int* maior, int* menor);
double calcula_media_matriz(int linha, int coluna, int** matriz);
void inicializa_matriz(int linha, int coluna, int** matriz);
void questao3();

int main()
{
	questao3();
	return 0;
}

int** aloca_matriz(int linha, int coluna)
{
	if (linha < 1 || coluna < 1)
	{
		printf("Parametro Invalido.\n");
		return NULL;
	}

	int** matriz = (int**)malloc(linha * sizeof(int*));//aloca as linha da mmatriz

	for (int i = 0; i < linha; i++)
	{
		matriz[i] = (int*)calloc(coluna, sizeof(int)); //aloca as colunas da matriz
	}

	return	matriz;
}

int** libera_matriz(int linha, int coluna, int** matriz)
{
	if (matriz == NULL)
		return NULL;

	if (linha < 1 || coluna < 1)
	{
		printf("Parametro Invalido.\n");
		return (matriz);
	}

	for (int i = 0; i < linha; i++)//libera as colunas
	{
		free(matriz[i]);
	}

	free(matriz);//libera as linhas

	return NULL;
}

// "retorna" o menor e o maior valor da matriz
void menor_maior(int linha, int coluna, int** matriz, int* maior, int* menor)
{
	*maior = INT_MIN;
	*menor = INT_MAX;

	for (int i = 0; i < linha; i++)
	{
		for (int j = 0; j < coluna; j++)
		{
			if (matriz[i][j] > * maior)
			{
				*maior = matriz[i][j];
			}

			if (matriz[i][j] < *menor)
			{
				*menor = matriz[i][j];
			}
		}
	}
}

double calcula_media_matriz(int linha, int coluna, int** matriz)
{
	double divi = linha * coluna;
	int soma = 0;

	for (int i = 0; i < linha; i++)
	{
		for (int j = 0; j < coluna; j++)
		{
			soma += matriz[i][j];
		}
	}

	double media = soma /divi ;

	return media;
}

void inicializa_matriz(int linha, int coluna, int** matriz)
{
	for (int i = 0; i < linha; i++)
	{
		for (int j = 0; j < coluna; j++)
		{
			scanf("%d", &matriz[i][j]);
		}
	}
}

void questao3()
{
	int nro_mtz;//quantidades de matrizes

	scanf("%d", &nro_mtz);

	for (int i = 0; i < nro_mtz; i++)
	{
		int nro_max, nro_min;
		double media;
		int  linha, coluna;
		scanf("%d", &linha);
		scanf("%d", &coluna);

		int** matriz = aloca_matriz(linha, coluna);
		inicializa_matriz(linha, coluna, matriz);
		menor_maior(linha, coluna, matriz, &nro_max, &nro_min);
		media = calcula_media_matriz(linha, coluna, matriz);

		printf("%d %d %.3lf\n", nro_max, nro_min, media);
	}
}