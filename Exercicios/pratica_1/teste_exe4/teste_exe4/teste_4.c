#include <stdio.h>
#include <stdlib.h>

int main()
{

}

int** aloca_matriz(int linha, int coluna)
{
	if (linha < 1 || coluna < 1)
	{
		printf("Parametro Invalido.\n");
		return NULL;
	}

	int** matriz = (int**)malloc(linha * sizeof(int*));//aloca as linha da mmatriz

	for (int i = 0; i < linha; i++)
	{
		matriz[i] = (int*)calloc(coluna, sizeof(int)); //aloca as colunas da matriz
	}

	return	matriz;
}

int** libera_matriz(int linha, int coluna, int** matriz)
{
	if (matriz == NULL)
		return NULL;

	if (linha < 1 || coluna < 1)
	{
		printf("Parametro Invalido.\n");
		return (matriz);
	}

	for (int i = 0; i < linha; i++)//libera as colunas
	{
		free(matriz[i]);
	}

	free(matriz);//libera as linhas

	return NULL;
}

int* aloca_vetor(int nro_questoes)
{
	if (nro_questoes < 1)
	{
		return NULL;
	}
	else
	{
		int* vetor = (int*)calloc(nro_questoes, sizeof(int));

		return vetor;
	}
}

int* libera_vetor(int** vetor)
{
	free(vetor);

	return NULL;
}

void questao_4()
{
	int nro_questoes, nro_alunos;
	int linha, coluna;
	int** matriz;
	int* vet, * vet_alunos;//esse vet armazena a respostas do gabarito
	int aux = 0;// quantidade ques quatoes acerto

	scanf("%d", &nro_questoes);
	scanf("%d", &nro_alunos);

	coluna = nro_questoes + 1;
	linha = nro_alunos;

	vet = aloca_vetor(nro_questoes);

	for (int i = 0; i < nro_questoes; i++)
	{
		scanf("%d", &vet[i]);
	}

	for (int i = 0; i < linha; i++)
	{
		for (int j = 0; j < coluna; j++)
		{
			scanf("%d", &matriz[i][j]);
		}
	}

}