//pratica 1 
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <limits.h>

#define RADIANOS  0.0174532925

long double fatoreal(int number);
double degrees_to_radian(double graus);
void coss(int nro_termos);

int** aloca_matriz(int linha, int coluna);
int** libera_matriz(int linha, int coluna, int** matriz);
void menor_maior(int linha, int coluna, int** matriz, int* maior, int* menor);
double calcula_media_matriz(int linha, int coluna, int** matriz);
void inicializa_matriz(int linha, int coluna, int** matriz);
int* aloca_vetor(int nro_questoes);
int* libera_vetor(int* vetor);
void resultado(int** matriz, int* vetor, int linha, int coluna, int nro_questoes);

void questao1();
void questao2();
void questao3();
void questao4();

void menu();

int main()
{
	menu();

	return 0;
}

//calcula o fatoreal 
long double fatoreal(int number)
{
	long double num = 1; //numero auxiliar

	for (int i = 1; i <= number; i++)
	{
		num = num * i;
	}

	return num;
}

//coverte graus para radianos
double degrees_to_radian(double graus)
{
	double radianos = graus * RADIANOS;

	return radianos;
}

//faz toda a aritimetica
void coss(int nro_termos)
{
	for (int i = 0; i < nro_termos; i++)
	{
		double angulo;
		int num_interacao;//quantas vezes irá interagir
	   // scanf("%d", &angulo);
		//cin >> angulo;
		scanf("%lf", &angulo);
		scanf("%d", &num_interacao);
		long double cosseno;
		int expoente = 0;

		//editado aqui
		angulo *= RADIANOS;


		for (int j = 1; j <= num_interacao; j++)
		{
			if (j == 1)
			{
				cosseno = 1;
			}
			else
			{
				if (j % 2 != 0)
				{
					expoente += 2;
					cosseno += pow(angulo, expoente) / fatoreal(expoente);
				}
				else
				{
					expoente += 2;
					cosseno -= pow(angulo, expoente) / fatoreal(expoente);
				}


			}
		}

		printf("%.2lf ", angulo);
		printf("%d ", num_interacao);
		printf("%.5Lf\n", cosseno);

		/*cout << fixed << setprecision(2) << angulo << " " << num_interacao << " " << fixed << setprecision(5) << cosseno << endl;*/
	}
}

int** aloca_matriz(int linha, int coluna)
{
	if (linha < 1 || coluna < 1)
	{
		printf("Parametro Invalido.\n");
		return NULL;
	}

	int** matriz = (int**)malloc(linha * sizeof(int*));//aloca as linha da mmatriz

	for (int i = 0; i < linha; i++)
	{
		matriz[i] = (int*)calloc(coluna, sizeof(int)); //aloca as colunas da matriz
	}

	return	matriz;
}

int** libera_matriz(int linha, int coluna, int** matriz)
{
	if (matriz == NULL)
		return NULL;

	if (linha < 1 || coluna < 1)
	{
		printf("Parametro Invalido.\n");
		return (matriz);
	}

	for (int i = 0; i < linha; i++)//libera as colunas
	{
		free(matriz[i]);
	}

	free(matriz);//libera as linhas

	return NULL;
}

// "retorna" o menor e o maior valor da matriz
void menor_maior(int linha, int coluna, int** matriz, int* maior, int* menor)
{
	*maior = INT_MIN;
	*menor = INT_MAX;

	for (int i = 0; i < linha; i++)
	{
		for (int j = 0; j < coluna; j++)
		{
			if (matriz[i][j] > * maior)
			{
				*maior = matriz[i][j];
			}

			if (matriz[i][j] < *menor)
			{
				*menor = matriz[i][j];
			}
		}
	}
}

double calcula_media_matriz(int linha, int coluna, int** matriz)
{
	double divi = linha * coluna;
	int soma = 0;

	for (int i = 0; i < linha; i++)
	{
		for (int j = 0; j < coluna; j++)
		{
			soma += matriz[i][j];
		}
	}

	double media = soma / divi;

	return media;
}

void inicializa_matriz(int linha, int coluna, int** matriz)
{
	for (int i = 0; i < linha; i++)
	{
		for (int j = 0; j < coluna; j++)
		{
			scanf("%d", &matriz[i][j]);
		}
	}
}

int* aloca_vetor(int nro_questoes)
{
	if (nro_questoes < 1)
	{
		return NULL;
	}
	else
	{
		int* vetor = (int*)calloc(nro_questoes, sizeof(int));

		return vetor;
	}
}

int* libera_vetor(int* vetor)
{
	free(vetor);

	return NULL;
}

void resultado(int** matriz, int* vetor, int linha, int coluna, int nro_questoes)//linha é quantidade de alunos e coluna é a quantidade de questões mais 1 (nro de chamanda do aluno)
{
	int aux;// quantidade ques quatoes acerto

	for (int i = 0; i < linha; i++)
	{
		aux = 0;

		printf("%d ", matriz[i][coluna - 1]);
		for (int j = 0; j < coluna; j++)
		{
			if (matriz[i][j] == vetor[j])
			{
				aux++;
			}
		}
		printf("%d\n", aux);
	}
}

void questao1()
{
	int nro_termos;
	scanf("%d", &nro_termos);

	coss(nro_termos);
}

void questao2()
{
	double xa, da, ra;
	double xb, rb;
	int m;//meses
	int aux_m = 1;
	int intracoes;

	scanf("%d", &intracoes);

	for (int i = 0; i < intracoes; i++)
	{
		scanf("%lf %lf %lf %lf %lf %d", &xa, &da, &ra, &xb, &rb, &m);

		if (rb < ra)
		{
			printf("Rendimento de B Invalido\n");
			continue;
		}
		else
		{
			for (int j = 0; j < m; j++)
			{
				xa += (xa * (ra / 100));
				xa += da;

				xb += (xb * (rb / 100));

				if (xa > xb)
				{
					aux_m++;
				}
			}

			if (xa > xb)
			{
				printf("%.2lf %.2lf B nao supera A\n", xa, xb);
			}
			else
			{
				printf("%.2lf %.2lf %d\n", xa, xb, aux_m);
			}
		}
	}
}

void questao3()
{
	int nro_mtz;//quantidades de matrizes

	scanf("%d", &nro_mtz);

	for (int i = 0; i < nro_mtz; i++)
	{
		int nro_max, nro_min;
		double media;
		int  linha, coluna;
		scanf("%d", &linha);
		scanf("%d", &coluna);

		int** matriz = aloca_matriz(linha, coluna);
		inicializa_matriz(linha, coluna, matriz);
		menor_maior(linha, coluna, matriz, &nro_max, &nro_min);
		media = calcula_media_matriz(linha, coluna, matriz);

		printf("%d %d %.3lf\n", nro_max, nro_min, media);
	}
}

void questao4()
{
	int nro_questoes, nro_alunos;
	int linha, coluna;
	int* vet;//esse vet armazena a respostas do gabarito
	//int aux = 0;// quantidade ques quatoes acerto

	scanf("%d", &nro_questoes);
	scanf("%d", &nro_alunos);

	coluna = nro_questoes + 1;
	linha = nro_alunos;

	vet = aloca_vetor(nro_questoes);

	for (int i = 0; i < nro_questoes; i++)
	{
		scanf("%d", &vet[i]);
	}

	int** matriz = aloca_matriz(linha, coluna);

	inicializa_matriz(linha, coluna, matriz);

	resultado(matriz, vet, linha, coluna, nro_questoes);

	libera_vetor(vet);
	libera_matriz(linha, coluna, matriz);
}

void menu()
{
	int opcao;

	do {
		scanf("%d", &opcao);
		//printf("\n%d\n", opcao);

		switch (opcao)
		{
		case 1://atividade 1

			printf("\nQUESTAO 1:\n");
			questao1();

			break;

		case 2://atividade 2

			printf("\nQUESTAO 2:\n");
			questao2();

			break;

		case 3://atividade 3

			printf("\nQUESTAO 3:\n");
			questao3();

			break;

		case 4://ativide 4

			printf("\nQUESTAO 4:\n");
			questao4();

			break;
		default:

			//printf("Erro");
			break;
		}

	}while(opcao != 0);
}

//primeira linha da input vai ser a opção do menu;
//e abaixo será a o input do exercicio em si