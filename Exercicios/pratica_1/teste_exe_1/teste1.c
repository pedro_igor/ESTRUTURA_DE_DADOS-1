#include <stdio.h>
#include <math.h>

#define RADIANOS  0.0174532925

long double fatoreal(int number);
double degrees_to_radian(double graus);
void coss(int nro_termos);
void questao1();

int main()
{
	questao1();

	return 0;
}

//calcula o fatoreal 
long double fatoreal(int number)
{
	long double num = 1; //numero auxiliar

	for (int i = 1; i <= number; i++)
	{
		num = num * i;
	}

	return num;
}

double degrees_to_radian(double graus)
{
	double radianos = graus * RADIANOS;

	return radianos;
}

void coss(int nro_termos)
{
	for (int i = 0; i < nro_termos; i++)
	{
		double angulo;
		int num_interacao;//quantas vezes ir� interagir
	   // scanf("%d", &angulo);
		//cin >> angulo;
		scanf("%lf", &angulo);
		scanf("%d", &num_interacao);
		long double cosseno;
		int expoente = 0;

		//editado aqui
		angulo *= RADIANOS;


		for (int j = 1; j <= num_interacao; j++)
		{
			if (j == 1)
			{
				cosseno = 1;
			}
			else
			{
				if (j % 2 != 0)
				{
					expoente += 2;
					cosseno += pow(angulo, expoente) / fatoreal(expoente);
				}
				else
				{
					expoente += 2;
					cosseno -= pow(angulo, expoente) / fatoreal(expoente);
				}


			}
		}

		printf("%.2lf ", angulo);
		printf("%d ", num_interacao);
		printf("%.5Lf\n", cosseno);

		/*cout << fixed << setprecision(2) << angulo << " " << num_interacao << " " << fixed << setprecision(5) << cosseno << endl;*/
	}
}

void questao1()
{
	int nro_termos;
	scanf("%d", &nro_termos);

	coss(nro_termos);
}