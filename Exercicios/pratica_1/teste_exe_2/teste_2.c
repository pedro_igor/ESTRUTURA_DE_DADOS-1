#include <stdio.h>

void questao2();

int main()
{
	questao2();

	return 0;
}

void questao2()
{
	double xa, da, ra;
	double xb, rb;
	int m;//meses
	int aux_m = 1;
	int intracoes;

	scanf("%d", &intracoes);

	for (int i = 0; i < intracoes; i++)
	{
		scanf("%lf %lf %lf %lf %lf %d", &xa, &da, &ra, &xb, &rb, &m);

		if (rb < ra)
		{
			printf("Rendimento de B Invalido\n");
			continue;
		}
		else
		{
			for (int j = 0; j < m; j++)
			{
				xa += da;
				xa += (xa * (ra / 100));

				xb += (xb * (rb / 100));

				if (xa > xb)
				{
					aux_m++;
				}
			}

			if (xa > xb)
			{
				printf("%.2lf %.2lf B nao supera A\n", xa, xb);
			}
			else
			{
				printf("%.2lf %.2lf %d\n", xa, xb, aux_m);
			}
		}
	}
}