//funcoes recursivas
#include "funcoes_recursivas.hpp"

int soma_vetor(int* vetor, int n)
{
	if (n == 0)
	{
		return 0;
	}
	else
	{
		return vetor[n - 1] + soma_vetor(vetor, n - 1);
	}
}

void troca_vetor(int* vetor, int inicio, int final)
{
	if (inicio >= final)
	{
		return;
	}
	else
	{
		int aux = vetor[inicio];
		vetor[inicio] = vetor[final - 1];
		vetor[final - 1] = aux;
		troca_vetor(vetor, inicio += 1, final -= 1);
	}
}

int mod(int x, int y)
{
	if (x == y)
	{
		return 0;
	}
	else if(x < y)
	{
		return x;
	}

	
	return mod(x - y, y);
	
}

int somatorio(int n)
{
	if (n == 1)
	{
		return 1;
	}
	else
	{
		return n + somatorio(n - 1);
	}
}