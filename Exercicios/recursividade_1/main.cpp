#include "funcoes_recursivas.hpp"

void menu();
int* preencher_vetor(int tam);
void deleta_vetor(int* vetor);
void print_vetor(int* vetor, int tam);

int main()
{
	int opcao;

	int* vetor;
	int tam;//tamanho
	int soma = 0;

	int x, y;
	int n;
	//int mod;

	do
	{
		cout << "\nDigite uma opcao: ";
		cin >> opcao;

		switch (opcao)
		{
			case 1:
				
				cout << "Questao 1" << endl;

				cout << "\nQual eh o tamanho do vetor: ";
				cin >> tam;

				cout << "\nDigite os elementos do vetor: ";

				vetor = preencher_vetor(tam);

				soma = soma_vetor(vetor, tam);

				cout << "\nA soma dos elementos do vetor: " << soma << endl;

				deleta_vetor(vetor);

				break;

			case 2:

				cout << "Questao 2" << endl;

				cout << "\nQual eh tamanho do vetor: ";
				cin >> tam;

				cout << "\nDigite os elemntos do vetor: ";

				vetor = preencher_vetor(tam);

				troca_vetor(vetor, 0, tam);

				print_vetor(vetor, tam);

				deleta_vetor(vetor);

				break;

			case 3:

				cout << "Questao 3 " << endl;

				cout << "\nDigite os valores de x e y: ";
				cin >> x >> y;

				cout << mod(x, y) << endl;

				break;

			case 4:

				cout << "Questao 4" << endl;

				cout << "\nDigite o valor de n: \n";
				cin >> n;

				cout << "\nO somatorio de n = " << somatorio(n);

				break;

			case 0:

				cout << "VOCE SAIU\n";
				break;

			default:

				cout << "ERRO: opcao invalida" << endl;
				break;
		}
	} while (opcao != 0);

	return 0;
}

void menu()
{
	cout << "\n1 - Questao 1"
		<< "\n2 - Questao 2"
		<< "\n3 - Questao 3"
		<< "\n4 - Questao 4"
		<< "\n0 - Sair" << endl;
}

int* preencher_vetor(int tam)
{
	int* vetor = new int[tam];
	for (int i = 0; i < tam; i++)
	{
		cin >> vetor[i];
	}

	return vetor;
}

void deleta_vetor(int* vetor)
{
	delete vetor;
}

void print_vetor(int* vetor, int tam)
{
	for (int i = 0; i < tam; i++)
	{
		cout << vetor[i] << " " << endl;
	}
}