#ifndef FUNCOES_RECURSIVAS_HPP
#define FUNCOES_RECURSIVAS_HPP

#include <iostream>

using namespace std;

int soma_vetor(int* vetor, int n);

void troca_vetor(int* vetor, int inicio, int final);

int mod(int x, int y);

int somatorio(int n);

#endif // !FUNCOES_RECURSIVAS_HPP
