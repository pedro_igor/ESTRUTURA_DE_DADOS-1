#ifndef ALUNO_H
#define ALUNO_H

typedef struct aluno Aluno;

Aluno* cira_aluno(char *nome, long int cpf, int matricula, char* curso,char *data);

void libera_aluno(Aluno* aluno);

//Pessoa* retorna_dados(Aluno* aluno);

char* retorna_nome(Aluno* aluno);

long int retorna_cpf(Aluno* aluno);

int retorna_matricula(Aluno* aluno);

char* retorna_curso(Aluno* aluno);

char* retorna_data_ingresso(Aluno* aluno);

void atualiza_aluno(Aluno* aux,char *nome, long int cpf, int matricula, char* curso,char *data);

void print_aluno(Aluno* aluno);

#endif // !ALUNO_H

// set = atualizar
// get = ler