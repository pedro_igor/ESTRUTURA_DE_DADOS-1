//aluno.cpp
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "aluno.h"

struct aluno
{
	//Pessoa* dados_aluno;
	char nome[100];
	long int cpf;
	int matricula;
	char curso[100];
	//Data* data_ingreso;
	char data[12];
};

Aluno* cira_aluno(char *nome, long int cpf, int matricula, char* curso,char *data)
{
	Aluno* aux = malloc(sizeof(Aluno));

	if (aux == NULL)
	{
		printf(" Erro: Memoria insuficiente.\n");
		exit(1);
	}

	strcpy(aux->nome, nome);
	aux->cpf = cpf;
	aux->matricula = matricula;
	strcpy(aux->curso, curso);
	strcpy(aux->data, data);
	return aux;
}

void libera_aluno(Aluno* aluno)
{	
	free(aluno);
}

/*Pessoa* retorna_dados(Aluno* aluno)
{
	return aluno->dados_aluno;
}*/

char* retorna_nome(Aluno* aluno)
{
	return aluno->nome;
}

long int retorna_cpf(Aluno* aluno)
{
	return aluno->cpf;
}
int retorna_matricula(Aluno* aluno)
{
	return aluno->matricula;
}

char* retorna_curso(Aluno* aluno)
{
	return aluno->curso;
}

char* retorna_data_ingresso(Aluno* aluno)
{
	return aluno->data;
}

void atualiza_aluno(Aluno* aux,char *nome, long int cpf, int matricula, char* curso,char *data)
{
    strcpy(aux->nome, nome);
	aux->cpf = cpf;
	aux->matricula = matricula;
	strcpy(aux->curso, curso);
	strcpy(aux->data, data);	
}

void print_aluno(Aluno* aluno)
{
	printf("Nome: %s\n", aluno->nome);
	printf("CPF: %ld\n", aluno->cpf);
	printf("Matricula: %d\n", aluno->matricula);
	printf("Data de ingresso: %s\n", retorna_data_ingresso(aluno) );
	printf("Curso: %s", aluno->curso);
}
