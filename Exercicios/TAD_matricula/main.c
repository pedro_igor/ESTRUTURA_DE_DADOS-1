//main.c; main da TAD matricula
#include "atestado_Matricula.h"
#include "aluno.h"
#include "disciplina.h"
#include <stdio.h>
#include <string.h>

//void menu_inicial();
//void menu_aluno(Aluno** aluno);
//void menu_disciplina(Disciplina** disciplina);

void opcoes_inicial();

int main()
{
	
	////int quan_a;//quantidade de alunos
	//int quant_d;//quantidade de disciplina

	//printf("QUAL EH A QUANTIDADE DE ALUNOS:\n");
	//scanf("%d", &quan_a);

	/*menu_inicial();*/

	//teste inicial

	Atestado* atestado;

	int ch_atestado = 0;

	Aluno* aluno;
	char nome[100];//nome do aluno
	long int cpf;//cpf do aluno
	int matricula;//nome da disciplina
	char data[100];//data de ingresso ou data de nascimento
	//char* rua, * bairro, * cidade, * estado;// endereco do aluno
	//int cont_aluno = 0;
	char curso[100];//nome do curso que o aluno faz

	Disciplina** disciplina;
	char nome_diciplina[100];//nome da disciplina
	int cod;//codgo da disciplina
	char departamento[100]; // ou curso
	int sala;//sala onde sera lecionada
	int ch;//carga-horaria
	int aux_disciplina = 0;

	//int quant_d;//quantidade de disciplina

	//int matricula_aux;
	//int aux_cod;

	

	int op;
	
	do{
		opcoes_inicial();
		scanf("%d", &op);

		switch (op)
		{
			//cria atestado
			case 1:
			{
				printf("\nCRIAR ALUNO:\n");
					
				printf("\nNOME DO ALUNO: ");
				scanf("%s", nome);

				printf("\nMATRICULA: ");
				scanf("%d", &matricula);

				printf("\nCURSO DO ALUNO: ");
				scanf("%s", curso);

				printf("\nCPF: ");
				scanf("%ld", &cpf);

				printf("\nDATA DE INGRESO (dia/mes/ano): ");
				scanf("%s", data);

				aluno = cira_aluno(nome, cpf, matricula, curso, data);

				//print_aluno(aluno);
				printf("\n\nQual é a quantidade de disciplinas: ");

				do
				{
					scanf("%d", &aux_disciplina);

				} while (aux_disciplina > 4);
				
				//printf("%d\n", retorna_matricula(aluno));
				
				disciplina = cira_disciplinas(aux_disciplina);

				for (int i = 0; i < aux_disciplina; i++)
				{
					printf("\nNOME DA DISCIPLINA %d:", i + 1);
					scanf("%s", nome_diciplina);

					printf("\nCODIGO: ");
					scanf("%d", &cod);

					printf("\nDEPARTAMENTO: ");
					scanf("%s",departamento);

					printf("\nCARGA HORARIA: ");
					ch = 4; //4 horas eh o padoao
					printf("%d", ch);

					ch_atestado += ch;

					printf("\nSALA: ");
					scanf("%d", &sala);

					disciplina[i] = cria_disciplina(nome_diciplina, cod, ch, departamento, sala);

					//aux_disciplina++;
				}

				atestado = cria_atestado(aluno, disciplina, ch_atestado);
				
				break;
			}
			case 2:{
				printf("ALTERAR ALUNO:\n");
	
				printf("\nNOME DO ALUNO: ");
				scanf("%s", nome);

				printf("\nMATRICULA: ");
				scanf("%d", &matricula);

				printf("\nCURSO DO ALUNO: ");
				scanf("%s", curso);

				printf("\nCPF: ");
				scanf("%ld", &cpf);

				printf("\nDATA DE INGRESO (dia/mes/ano): ");
				scanf("%s", data);

				atualizar_aluno(atestado, aluno);
	
				break;
			}
			case 3:{
				printf("ALTERAR DISCIPLINA:\n");

				printf("DIGITE O CODIGO DA DISCIPLINA PARA ALTERA-LA: ");
				scanf("%d", &cod);

				for (int i = 0; i < aux_disciplina; i++)
				{
					if (cod == retorna_cod(retornaa_disciplina(retorna_disciplinas(atestado),i)))
					{
						printf("\nNOME DA DISCIPLINA %d:", i + 1);
						scanf("%s", nome_diciplina);

						printf("\nCODIGO: ");
						scanf("%d", &cod);

						printf("\nDEPARTAMENTO: ");
						//fgets(departamento, 30, stdin);
						scanf("%s", departamento);

						printf("\nCARGA HORARIA: ");
						ch = 4; //4 horas eh o padoao

						printf("\nSALA: ");
						scanf("%d", &sala);

						atualiza_disciplia(disciplina[i], nome_diciplina, cod, ch, departamento, sala);

						atualizar_disciplina(atestado, disciplina[i], i);

						break;
					}
				}

				break;
			}
			case 4:{
				printf("\nIMPRIMIR INFORMACOES\n\n");

				print_atestado(atestado);

				break;
			}
			case 5:{
				printf("DELETAR ATESTADO:\n");

				libera_atestado(atestado);
				printf("Atestado deletado com sucesso\n");

				break;
			}
			default:
				break;
		}
	} while(op != 0);

	printf("\nVOCE SAIU\n");


	return 0;
}

//opcoes no menu inicial

void opcoes_inicial()
{
	printf("\nDIGITE UMA OPCAO:\n");
	printf("\n");
	printf("1 - CRIA ATESTADO\n");
	printf("2 - ALTERA ALUNO\n");
	printf("3 - ALTERA DISCIPLINA\n");
	printf("4 - IMPRIMIR INFORMACOES\n");
	printf("5 - APAGAR ATESTADO\n");
	printf("0 - SAIR\n");
}

