//disciplina.h

#include "disciplina.h"

struct disciplina
{
	char nome_dis[20];
	int codigo;
	int carga_hora;
	char curso[20];
	int sala;
};

Disciplina** cira_disciplinas(int quant_disciplina)
{
	return malloc(quant_disciplina * sizeof(Disciplina*));
}
Disciplina* cria_disciplina(char* nome, int cod, int carga_h, char* curso, int sala)
{
	Disciplina* dic = malloc(sizeof(Disciplina)); // Disciplina* dic = (Disciplina*)malloc(sizeof(Disciplina));

	if (dic == NULL) 
	{
		printf("ERRO: nao ha memoria suficiente.\n");
		exit(1);
	}

	strcpy(dic->nome_dis, nome);
	dic->codigo = cod;
	dic->carga_hora = carga_h;
	strcpy(dic->curso, curso);
	dic->sala = sala;

	return dic;
}

void liberar_discilina(Disciplina* disciplina)
{
	free(disciplina->nome_dis);
	free(disciplina->curso);
	free(disciplina);
}

char* retorna_nome_disciplina(Disciplina* disciplina)
{
	return disciplina->nome_dis;
}

int retorna_cod(Disciplina* disciplina)
{
	return disciplina->codigo;
}

int retorna_ch(Disciplina* disciplina)
{
	return disciplina->carga_hora;
}

char* retorna_curso_d(Disciplina* disciplina)
{
	return disciplina->curso;
}

int retorna_sala(Disciplina* disciplina)
{
	return disciplina->sala;
}

void atualiza_disciplia(Disciplina* dic, char* nome, int cod, int carga_h, char* curso, int sala)
{
	strcpy(dic->nome_dis, nome);
	dic->codigo = cod;
	dic->carga_hora = carga_h;
	strcpy(dic->curso, curso);
	dic->sala = sala;
}

Disciplina* retornaa_disciplina(Disciplina** disciplina, int pos)
{
	return disciplina[pos];
}

void print_disciplina(Disciplina* disciplina)
{
	printf("\nNome da Disciplina: %s\n", disciplina->nome_dis);
	printf("Codigo: %d\n", disciplina->codigo);
	printf("Carga horaria: %d h\n", disciplina->carga_hora);
	//printf("Curso: %s", disciplina->curso);
	printf("Sala: %d\n", disciplina->sala);
}