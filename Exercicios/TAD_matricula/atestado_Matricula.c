//atestado_Matricula.c

#include "atestado_Matricula.h"


struct atestado
{
	Aluno* aluno;
	Disciplina** disciplinas;
	int cargar_horaria;
};

Atestado* cria_atestado(Aluno* aluno, Disciplina** disciplina, int horas)
{
	Atestado* aux = malloc(sizeof(Atestado));

	if (aux == NULL)
	{
		printf("ERRO: memoria insuficiente.\n");
	}

	aux->aluno = aluno;
	aux->disciplinas = disciplina;
	aux->cargar_horaria = horas;

	return aux;
}

void libera_atestado(Atestado* atestado)
{
	free(atestado->aluno);
	for (int i = 0; i < 4; i++)
	{
		free(retornaa_disciplina(atestado->disciplinas, i));
	}
	free(atestado);
}

void atualizar_aluno(Atestado* atestado, Aluno* aluno)
{
	atestado->aluno = aluno;
}

void atualizar_disciplina(Atestado* atestado, Disciplina* disciplina, int pos)
{
	atestado->disciplinas[pos] = disciplina;
}

void atualizar_horas(Atestado* atestado, int horas)
{
	atestado->cargar_horaria = horas;
}

int retorna_horas(Atestado* atestado)
{
	return atestado->cargar_horaria;
}

Disciplina** retorna_disciplinas(Atestado* atestado)
{
	return atestado->disciplinas;
}


Aluno* retorna_aluno(Atestado* atestado)
{
	return atestado->aluno;
}

void print_atestado(Atestado* atestado)
{
	print_aluno(atestado->aluno);

	for (int i = 0; i < atestado->cargar_horaria; i+=4)
	{
		print_disciplina(retornaa_disciplina( atestado->disciplinas, i/4));
	}
}