//professor.c
#include <stdio.h>
#include <stdlib.h>
#include "professor.h"
#include "pessoa.h"

struct professor
{
	Pessoa* prfessor;
	char* departamento;
};

Teacher* cria_professor(Pessoa* professor, char* departamento)
{
	Teacher* aux = (Teacher*)malloc(sizeof(Teacher*));

	if (aux == NULL)
	{
		printf("ERRO: memoria insufiente.\n");
		exit(1);
	}

	aux->prfessor = professor;
	aux->departamento = departamento;

	return aux;
}

void libera_professor(Teacher* professor)
{
	free(professor->departamento);
	free(professor->prfessor);
	free(professor);
}

Pessoa* get_pessoa(Teacher* professor)
{
	return professor->prfessor;
}

char* get_depertamento(Teacher* professor)
{
	return professor->departamento;
}

void atualiza_professor(Teacher* professor, Pessoa* dados_professor, char* departamento)
{
	professor->prfessor = dados_professor;
	professor->departamento = departamento;
}
