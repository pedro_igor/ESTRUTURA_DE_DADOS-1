#ifndef DATA_H
#define DATE_H

typedef struct data Data;

Data* cria_date(int dia, int mes, int ano);

void libera_date(Data* date);

void acessa_date(Data* date, int* dia, int* mes, int* ano);

int retorna_dia(Data* date);

int retorna_mes(Data* date);

int retorna_ano(Data* date);

void atribui_date(Data* date, int dia, int mes, int ano);

#endif // !DATA_H
