//date.cpp
#include "data.h"
#include <stdio.h>
#include <stdlib.h>

struct data
{
	int dia;
	int mes;
	int ano;
};

Data* cria_date(int dia, int mes, int ano)
{
	Data* date = (Data*)malloc(sizeof(Data*));

	if (date == NULL)
	{
		printf("ERRO: Nao ha memoria suficiente.\n");
		
		exit(1);
	}

	date->dia = dia;
	date->mes = mes;
	date->ano = ano;

	return date;
}

void libera_date(Data* date)
{
	free(date);
}

void acessa_date(Data* date, int* dia, int* mes, int* ano)
{
	*dia = date->dia;
	*mes = date->mes;
	*ano = date->ano;
}

void atribui_date(Data* date, int dia, int mes, int ano)
{
	date->dia = dia;
	date->mes = mes;
	date->ano = ano;
}

int retorna_dia(Data* date)
{
	return date->dia;
	return 0;
}

int retorna_mes(Data* date)
{
	return date->mes;
}

int retorna_ano(Data* date)
{
	return date->ano;
}
