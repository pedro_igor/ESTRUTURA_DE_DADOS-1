#ifndef PROFESSOR_H
#define PROFESSOR_H

#include "pessoa.h"

typedef struct professor Teacher;

Teacher* cria_professor(Pessoa* professor, char* departamento);

void libera_professor(Teacher* professor);

Pessoa* get_pessoa(Teacher* professor);

char* get_depertamento(Teacher* professor);

void atualiza_professor(Teacher* professor, Pessoa* dados_professor, char* departamento);

#endif // !PROFESSOR_H
