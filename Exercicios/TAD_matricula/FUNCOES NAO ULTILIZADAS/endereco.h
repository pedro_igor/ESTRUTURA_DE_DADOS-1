#ifndef ENDERECO_H
#define  ENDEREO_H

typedef struct endereco End;

End* cria_End(char* rua, char* bairro, char* cidade, char* estado);

void libera_End(End*end);

char* ret_rua(End* end);

char* ret_bairro(End* end);

char* ret_cidade(End* end);

char* ret_estado(End* end);

void atribui_End(End* end, char* rua, char* bairro, char* cidade, char* estado);


#endif // !ENDERECO_H
