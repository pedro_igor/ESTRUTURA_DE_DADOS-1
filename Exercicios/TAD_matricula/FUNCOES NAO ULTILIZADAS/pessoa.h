#ifndef PESSOA_H
#define PESSOA_H

#include "data.h"
#include "endereco.h"

typedef struct pessoa Pessoa;

Pessoa* cria_pessoa(char* nome, long int cpf/*, End* end, Data* data_nascimento*/);

void libera_pessoa(Pessoa* pessoa);

//void acessa_pessoa(Pessoa* pessoa, char* nome, long int* cpf);

char* retorna_nome(Pessoa* pessoa);

long int retorna_cpf(Pessoa* pessoa);

//End* retorna_end(Pessoa* pessoa);
//
//Data* retorna_data(Pessoa* pessoa);

void altribui_pessoa(Pessoa* pessoa, char* nome, long int cpf/*, End* end, Data* data_nascimento*/);

#endif // !PESSOA_H

