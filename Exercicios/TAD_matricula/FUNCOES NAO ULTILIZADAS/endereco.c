//endereco.cpp
#include <stdio.h>
#include <stdlib.h>
#include "endereco.h"

struct endereco
{
	char* rua;
	char* bairro;
	char* cidade;
	char* estado;
};

End* cria_End(char* rua, char* bairro, char* cidade, char* estado)
{
	End* endereco = (End*)malloc(sizeof(End*));

	if (endereco == NULL)
	{
		printf("ERRO: Memoria insuficiente.\n");
		
		exit(1);
	}

	endereco->rua = rua;
	endereco->bairro = bairro;
	endereco->cidade = cidade;
	endereco->estado = estado;

	return endereco;
}

void libera_End(End* end)
{
	free(end->rua);
	free(end->bairro);
	free(end->cidade);
	free(end->estado);
	free(end);
}

char* ret_rua(End* end)
{
	return end->rua;
}

char* ret_bairro(End* end)
{
	return end->bairro;
}

char* ret_cidade(End* end)
{
	return end->cidade;
}

char* ret_estado(End* end)
{
	return end->estado;
}

void atribui_End(End* end, char* rua, char* bairro, char* cidade, char* estado)
{
	end->rua = rua;
	end->bairro = bairro;
	end->cidade = cidade;
	end->estado = estado;
}
