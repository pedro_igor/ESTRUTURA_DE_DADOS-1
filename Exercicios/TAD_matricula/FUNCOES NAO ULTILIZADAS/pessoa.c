//pessoa.c
#include "pessoa.h"
#include "endereco.h"
#include "data.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct pessoa
{
	char* nome;
	long int cpf;
	//End* endereco;
	//Data* data_nascimento;
};

Pessoa* cria_pessoa(char* nome, long int cpf/*, End* end, Data* data_nascimento*/)
{
	Pessoa* person = (Pessoa*)malloc(sizeof(Pessoa*));

	if (person == NULL)
	{
		printf("ERRO: Memoria insufiente.\n");

		exit(1);
	}
	person->nome = nome;
	person->cpf = cpf;
	/*person->endereco = end;
	person->data_nascimento = data_nascimento;*/

	return person;
}

void libera_pessoa(Pessoa* pessoa)
{
	free(pessoa->nome);
	//free(pessoa->endereco);
	//free(pessoa->data_nascimento);
	free(pessoa);
}

char* retorna_nome(Pessoa* pessoa)
{
	return pessoa->nome;
}

long int retorna_cpf(Pessoa* pessoa)
{
	return pessoa->cpf;
}

//End* retorna_end(Pessoa* pessoa)
//{
//	return pessoa->endereco;
//}
//
//Data* retorna_data(Pessoa* pessoa)
//{
//	return pessoa->data_nascimento;
//}

//void acessa_pessoa(Pessoa* pessoa, char* nome, long int* cpf)
//{
//	nome = pessoa->nome;
//	*cpf = pessoa->cpf;
//	//*end = pessoa->endereco;
//	//*data_nascimento = pessoa->data_nascimento;
//}

void altribui_pessoa(Pessoa* pessoa, char* nome, long int cpf/*, End* end, Data* data_nascimento*/)
{
	pessoa->nome = nome;
	pessoa->cpf = cpf;
	//pessoa->endereco = end;
	//pessoa->data_nascimento = data_nascimento;
}