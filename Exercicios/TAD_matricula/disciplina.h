#ifndef DISCIPLINA_H
#define DISCIPLINA_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct disciplina Disciplina;

Disciplina** cira_disciplinas(int quant_disciplina);

Disciplina* cria_disciplina(char* nome, int cod, int carga_h, char* curso, int sala);

void liberar_discilina(Disciplina* disciplina);

char* retorna_nome_disciplina(Disciplina* disciplina);

int retorna_cod(Disciplina* disciplina);

int retorna_ch(Disciplina* disciplina);

char* retorna_curso_d(Disciplina* disciplina);

int retorna_sala(Disciplina* disciplina);

void atualiza_disciplia(Disciplina* dic, char* nome, int cod, int carga_h, char* curso, int sala);

Disciplina* retornaa_disciplina(Disciplina** disciplina, int pos);

void print_disciplina(Disciplina* disciplina);

#endif // !DISCIPLINA_H
