#ifndef ATESTADO_MATRICULA_H
#define ATESTADO_MATRICULA_H

#include <stdio.h>
#include <stdlib.h>
#include "aluno.h"
#include "disciplina.h"

typedef struct atestado Atestado;

Atestado* cria_atestado(Aluno* aluno, Disciplina** disciplina, int horas);

void libera_atestado(Atestado* atestado);

void atualizar_aluno(Atestado* atestado, Aluno* aluno);

void atualizar_disciplina(Atestado* atestado, Disciplina* disciplina, int pos);

void atualizar_horas(Atestado* atestado, int horas);

int retorna_horas(Atestado* atestado);

Disciplina** retorna_disciplinas(Atestado* atestado);

//Disciplina* retorna_disciplina(Atestado* atestado, int pos);

Aluno* retorna_aluno(Atestado* atestado);

void print_atestado(Atestado* atestado);


#endif // !ATESTADO_MATRICULA_H
