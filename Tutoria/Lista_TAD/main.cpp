//main da primeira lista de tad da tutoria
#include "livro.hpp"
#include <stdlib.h>

void menu();

int main()
{
	system("clear");
	Livro** book;
	Livro* libro;
	string titulo;
	string autor;
	string genero;
	int ano;

	int qunt_livros;//quantidade de livros a ser cadastrado
	int opcao;//opcao para o menu

	int indice;

	do 
	{
		menu();
		cin >> opcao;

		switch (opcao)
		{
			case 1:
				cout << "\nCRIAR LIVRO\n";

				cout << "Qual eh a quantidade de livros que voce quer criar: ";
				cin >> qunt_livros;

				book = cria_livros(qunt_livros);

				for (int i = 0; i < qunt_livros; i++)
				{
					cin.ignore();
					cout << "\nQual eh o neme do livro " << i + 1 << ": ";
					getline(cin, titulo);

					cout << "Qual eh o autor: ";
					getline(cin, autor);

					cout << "Qual eh o genero: ";
					cin >> genero;

					cout << "Qual eh o ano de lancamento livro: ";
					cin >> ano;

					book[i] = criar_livro(titulo, autor, genero, ano);
				}

				break;

			case 2:
				cout << "\nATUALIZAR LIVRO\n";

				cout << "Digite o titulo do livro: ";
				getline(cin, titulo);
				cin.ignore();

				libro = busca_livro(book, titulo, qunt_livros, &indice);

				cin.ignore();
				cout << "\nQual eh o nome do livro: " ;
				getline(cin, titulo);

				cout << "Qual eh o autor: ";
				getline(cin, autor);

				cout << "Qual eh o genero: ";
				cin >> genero;

				cout << "Qual eh o ano de lancamento livro: ";
				cin >> ano;
	
				book[indice] = criar_livro(titulo, autor, genero, ano);


				break;

			case 3:
				cout << "\nDELETAR LIVRO\n";

				break;

			case 4:
				cout << "\nDELETAR TODOS OS LIVROS\n";

				libera_livro(book, qunt_livros);

				cout << "VOCE DELETOU TODOS OS LIVROS." << endl;

				break;

			case 5:
				cout << "\nIMPRIMIR INFORMACOES\n";

				cin.ignore();
				cout << "Digite o Titulo do livro: ";
				getline(cin, titulo);

				libro = busca_livro(book, titulo, qunt_livros, &indice);

				mostrar_livro_especifico(libro);

				break;

			case 0:
				break;
			default:
				cout << "ERRO: opcao invalida.\n";
				break;
		}


	} while (opcao != 0);

	return 0;
}

void menu()
{
	cout << "DIGITE UMA OPCAO:\n\n";
	cout << "1 - Criar livro\n"
		<< "2 - Atualizar livro\n"
		<< "3 - Deletar livro\n"
		<< "4 - Deletar todos os livros\n"
		<< "5- Mostrar livro especifico\n"
		<< "0 - Sair\n";
}