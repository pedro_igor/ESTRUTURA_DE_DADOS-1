#ifndef LIVRO_HPP
#define LIVRO_HPP

#include <iostream>
#include <cstring>
#include <cstdlib>
#include <string>

using namespace std;
using std::string; 

typedef struct book Livro;

Livro** cria_livros(int quantidade_de_livros);

Livro* criar_livro(string titulo, string autor, string genero, int ano);

void libera_livro(Livro** book, int quantidade_livro);

void set_title(Livro* book, string titulo);

void set_autor(Livro* book, string autor);

void set_genero(Livro* book, string genero);

void set_ano(Livro* book, int ano);

string get_title(Livro* book);

string get_autor(Livro* book);

string get_genero(Livro* book);

int  get_ano(Livro* book);

Livro* busca_livro(Livro** book, string titulo, int quant_book, int* indice);

void mostrar_livro_especifico(Livro* book);

#endif //!LIVRO_HPP