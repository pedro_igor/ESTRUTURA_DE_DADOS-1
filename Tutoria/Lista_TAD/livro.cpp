//livro.hpp
#include "livro.hpp"

struct book
{
	string titulo;
	string autor;
	string genero;
	int ano;
};

Livro** cria_livros(int quantidade_de_livros)
{
	return new Livro*[quantidade_de_livros];
}
Livro* criar_livro(string titulo, string autor, string genero, int ano)
{
	Livro* book = new Livro;

	if (book == NULL)
	{
		cout << "ERRO: memoria insuficienten\n";
		exit(1);
	}

	book->titulo = titulo;
	book->autor = autor;
	book->genero = genero;
	book->ano = ano;
	return book;
}

void libera_livro(Livro** book, int quantidade_livro)
{
	for (int i = 0; i < quantidade_livro; i++)
	{
		delete book[i];
	}

	delete book;
}

void set_title(Livro* book, string titulo)
{
	book->titulo = titulo;
}

void set_autor(Livro* book, string autor)
{
	book->autor = autor;
}

void set_genero(Livro* book, string genero)
{
	book->genero = genero;
}

void set_ano(Livro* book, int ano)
{
	book->ano = ano;
}

string get_title(Livro* book)
{
	return string(book->titulo);
}

string get_autor(Livro* book)
{
	return string(book->autor);
}

string get_genero(Livro* book)
{
	return string(book->genero);
}

int get_ano(Livro* book)
{
	return book->ano;
}

Livro* busca_livro(Livro** book, string titulo, int quant_book, int* indice)
{
	for (int i = 0; i < quant_book; i++)
	{
		if (book[i]->titulo == titulo)
		{
			*indice = i;
			return book[i];
		}
	}
	return NULL;
}

void mostrar_livro_especifico(Livro* book)
{
	if (book == NULL)
	{
		cout << "ERRO: Livro nao encontrado\n";
	}
	else
	{
		cout << "\nTitulo: " << book->titulo;
		cout << "\nAutor: " << book->autor;
		cout << "\nGenero: " << book->genero;
		cout << "\nAno: " << book->ano << endl;;
	}
}