#include <iostream>
using namespace std;

#define TAM 5

int pesquisaBinaria(int *v, int chave, int esquerda, int direita);
int multiplicacao(int x, int y);
void labitinto(int lin, int col, char matriz[5][5]);
long int fibonnaci_quant(int n, int &rep);
void print_matriz(char matriz[5][5]);
int nro_ocorrencia(int n, int k, int m);

int main()
{
    int n;
	//exercicio 1
	int v[10] = {1,2,3,4,5,6,7,8,9,10};
	int chave = 9;

	int encontrou = pesquisaBinaria(v, chave, 0, 10);
	if(encontrou == -1)
	{
	cout << "chave nao encotrada" << endl;
	}
	else
	cout <<"A chave eh: " << v[pesquisaBinaria(v, chave, 0, 10)] << endl << endl;

	int x = 6, y = 4;

	//exercicio 2
	int linha = 0, coluna = 0;
	char matrz[5][5] = {{'1', '1', '1', '1', 'x'},
						{'x', '1', '1', '1', 'x'},
						{'1', 'x', 'x', '1', 'x'},
						{'x', '1', '1', '1', 'x'},
						{'x', 'x', '1', '1', '1'}};

	labitinto(linha, coluna, matrz);

	print_matriz(matrz);

    //exercicio 3
    n = 762021192;
    int k = 2;
    int i = 0;

    cout << "O numero " << k << " ocorre " << i << nro_ocorrencia(n, k , i) << endl << endl;


	//exercicio 4
	cout << endl;
	cout << x << " * " << y << " = " << multiplicacao(x, y) << endl;

	//questao 6
	n = 4;
    int chamada = 0;

	cout << "fib( " << n << ") = " << fibonnaci_quant(n, chamada) 
		<<" , chamadas = " << chamada - 1 << endl;  
	return 0;
}

int pesquisaBinaria(int *v, int chave, int esquerda, int direita)
{
    int m;

  if(esquerda < direita)
  {
      m = (esquerda + direita) / 2;

      if(chave == v[m])
      {
        return m;
      }
      else
      {
        if(v[m] < chave)
        {
          return pesquisaBinaria(v,chave, m + 1, direita );
        }
        else
        {
          return pesquisaBinaria(v, chave, esquerda, m - 1);
        }
        
      }
      
  }
  else
  {
    return -1;
  }
  
    
}

int multiplicacao(int x, int y)
{
  if(x == 0 || y == 0) //se um dos numeros for 0 retorna 0, pois todo numero multiplicado por zero e 0
  {
    return 0;
  }
  else if(y == 1) // se um y for 1 retorna o , pois todo numero multiplicado por 1 é ele mesmo
  {
    return x;
  }
  else
  {
    return x + multiplicacao(x, y - 1);
  }
}

void labitinto(int lin, int col, char matriz[5][5])
{
  if(lin < 0 || col < 0 || lin > TAM - 1 || col > TAM - 1)
  {
    return;
  }
  else if(matriz[lin][col] == 'x')
  {
    return;
  }
  else if(matriz[lin][col] == 'o')
  {
    return; 
  }
  
  matriz[lin][col] = 'o';

  labitinto(lin + 1, col, matriz);
  labitinto(lin, col + 1, matriz);
  labitinto(lin -1, col, matriz);
  labitinto(lin, col - 1, matriz);
  
}

//0 1 1 2 3 5

long int fibonnaci_quant(int n, int &rep)
{
	if(n == 0 || n == 1) //casos bases
	{
		rep++;
		return n;
	}
	else
	{
		rep++;
		return fibonnaci_quant(n - 1, rep) + fibonnaci_quant(n - 2, rep); 
	}
	
}

void print_matriz(char matriz[5][5])
{
	for(int i = 0; i < TAM; i++)
	{
		for(int j = 0; j < TAM; j++)
		{
			cout << matriz[i][j] << " ";
		}
		cout << endl;
	}
}

int nro_ocorrencia(int n, int k, int m)
{
    if(n % 10 == m)
    {
        m++;
    }
    else if (n % 10 == n)
    {
        return m;
    }

    nro_ocorrencia(n/10, k, m);
}
