#include "lista.h"

struct lista
{
    ListaNo* prim;
};

struct listano
{
    int info;
    ListaNo* prox;
};

/*funcao de criacao: retorna uma lista inicialmente vazia*/
Lista* lst_cria(void)
{
    Lista* l = (Lista*) malloc(sizeof(Lista));
    l->prim = NULL;

    return l;
}

/*insercao no inicio*/
void lst_insere(Lista* l, int v)
{
    ListaNo* novo = (ListaNo*) malloc(sizeof(ListaNo));
    novo->info = v;
    novo->prox = l->prim;
    l->prim = novo;
}

/*Funcao imprime: imprime valores dos elementos*/
void lst_imprime(Lista* l)
{
    for(ListaNo* p=l->prim; p != NULL; p=p->prox)
        printf("Info = %d\n", p->info);
}