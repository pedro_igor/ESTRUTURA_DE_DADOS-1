//main.c do exemplo 1 TAD ponto do livro

/********************************************************************************
** TAD ponto                                                                  **
** CRIA: opera��o que cria um ponto com coordenadas x e y;                    **
** LIBERA: opera��o que libera a mem�ria alocada por um ponto;                **
** ACESSA: opera��o que devolve as coordenadas de um ponto;                   **
** ATRIBUI: opera��o que atribui novos valores �s coordenadas de um ponto;    **
** DISTANCIA:opera��o que calcula a dist�ncia entre dois pontos.              **
********************************************************************************/

#include <stdio.h>
#include "ponto.h"

int main()
{
	Ponto* pt;
	float x = 2, y = 3.0;

	pt = cria(x, y);


	atribui(pt, x, y);

	libera(pt);

	return 0;
}