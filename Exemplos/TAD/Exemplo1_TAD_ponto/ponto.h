// TAD: Ponto (x,y)

// Tipo exportado 
typedef struct ponto Ponto;

// fun��es exportadas

/* Fun��o cria
** Aloca e retorna um ponto como coordernadas (x,y)
*/
Ponto* cria(float x, float y);

/*fun��o libera*/
void libera(Ponto* p);

//fun��o acessa
void acessa(Ponto* p, float* x, float* y);

//fun��o atribui 
void atribui(Ponto* p, float x, float y);

/* fun��o distancia 
retorna a distancia entre dois ponotos*/
float distancia(Ponto* p1, Ponto* p2);