//retorna o maior elemento de um vetor recursivo
#include <iostream>
using namespace std;

int maior_elemento(int* v, int tam);

int main()
{
	int v[5] = { 1,7,3,4,5 };

	cout << maior_elemento(v, 5) << endl;

	return 0;
}

int maior_elemento(int* v, int tam)
{
	if (tam == 1)
	{
		return v[0];
	}
	else
	{
		int aux = maior_elemento(v, tam - 1);

		if (aux > v[tam - 1])
		{
			return aux;
		}
		else
		{
			return v[tam - 1];
		}
	}
}