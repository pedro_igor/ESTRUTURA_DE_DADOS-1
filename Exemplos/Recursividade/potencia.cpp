//cacula potencia em recurciva
#include <iostream>
using namespace std;

int potencia(int base, int expo);

int main()
{
	int x, y;

	cin >> x >> y;

	cout << potencia(x, y) << endl;

	return 0;
}

int potencia(int base, int expo)
{
	if (expo <= 0)
	{
		return 1;
	}
	else
	{
		return base * potencia(base, expo - 1);
	}
}