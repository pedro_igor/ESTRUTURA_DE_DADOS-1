#ifndef MORADOR_H
#define MORADOR_H

#include <cstdio>
#include <cstdlib>
#include <string>
#include <iostream>
#include "financas.hpp"

using namespace std;
using std::string;

typedef struct morador Morador;

Morador** cria_moradores(int quant_moradores);

Morador* cria_morador(/*string nome, int idade, long int cpf, char sexo, string curso, Financa** financa, bool pagou*/);

void libera_moradores(Morador** morador, int quantidade_moradores);

void set_nome_morador(Morador* morador, string nome);

void set_idade(Morador* morador, int idade);

void set_cpf(Morador* morador, long int cpf);

void set_sexo(Morador* morador, char sexo);

//void set_tarefas(Morador* morador, Tarefas* tarefa);

void set_curso(Morador* morador, string curso);

void set_conta(Morador* morador, Financa* financa);

void set_pagou_conta(Morador* morador, bool conta);

string get_nome_morador(Morador* morador);

int get_idade(Morador* morador);

long int get_cpf(Morador* morador);

char get_sexo(Morador* morador);

//Tarefa* get_tarefa(Morador* morador);

string get_curso(Morador* morador);

bool get_pagou_conta(Morador* morador);

Financa* get_conta(Morador* morador);

int busca_morador(Morador** moradores, int quant_moradores, string nome_morador);

void ordenar_moradores(Morador** moradores, int quant_morador);

void merge(Morador** vetor, int comeco, int meio, int fim);

void mergeSort(Morador** vetor, int comeco, int fim);

int PesquisaBinaria (string  nome, Morador** v, int e, int d);

void print_morador(Morador* morador);

#endif // !MORADOR_H
