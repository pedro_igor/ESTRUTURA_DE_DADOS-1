﻿//funcoes do tipo void, para menu
#include "menus.hpp"

void menu_principal()
{
	//system("clear");
	cout << "\nEscolha uma opcao:\n"
		<< "╔═════════════════════╗\n"
		<< "║1 - Republica        ║\n"
		<< "║2 - Morador          ║\n"
		<< "║3 - Evento           ║\n"
		<< "║0 - Sair do Sistema  ║\n"
		<< "╚═════════════════════╝\n" << endl;
}

void menu_republica()
{
	//system("clear");
    cout << "\nREPUBLICA\n"
		<< "╔═══════════════════════════════════════╗\n"
        << "║ 1. Criar Republica                    ║\n"
        << "║ 2. Deletar Republica                  ║\n"
        << "║ 3. Atualizar Rebliblica               ║\n"
        << "║ 4. Imprimir Informacoes da Republica  ║\n"
        << "║-1. Voltar ao Menu Anterior            ║\n" 
		<< "╚═══════════════════════════════════════╝\n" << endl;
}

void menu_morador()
{
	//system("clear");
    cout << "\nMORADOR\n"
		<< "╔═══════════════════════════════════════╗\n"
        << "║ 1. Criar Morador                      ║\n"
        << "║ 2. Deletar todos os Moradores         ║\n"
        << "║ 3. Atualizar Morador                  ║\n"
        << "║ 4. Imprimir Informacoes do Morador    ║\n"
        << "║-1. Voltar ao Menu Anterior            ║\n" 
		<< "╚═══════════════════════════════════════╝\n" << endl;
}

void menu_evento()
{
	//system("clear");
    cout << "\nEVENTOS\n"
		<< "╔═══════════════════════════════════════╗\n"
        << "║ 1. Criar Evento                       ║\n"
        << "║ 2. Deletar Evento                     ║\n"
        << "║ 3. Atualizar Evento                   ║\n"
        << "║ 4. Imprimir Informacoes do Evento     ║\n"
        << "║-1. Voltar ao Menu Anterior            ║\n" 
		<< "╚═══════════════════════════════════════╝\n" << endl;
}
