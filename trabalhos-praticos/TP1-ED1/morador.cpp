#include "morador.hpp"
//#include "republica.h"

#include <string>
using std::string;

struct morador
{
	string nome;
	int idade;
	long int cpf;
	char sexo;
	//Tarefa** tarefa;
	string curso;
	Financa* contas;
	bool pagou;
};

Morador** cria_moradores(int quant_moradores)
{
	//return malloc(quant_moradores * sizeof(Morador*));
	return new Morador * [quant_moradores];
}

Morador* cria_morador(/*string nome, int idade, long int cpf, char sexo, string curso, Financa** financa, bool pagou*/)
{
	Morador* aux = new Morador;

	if (aux == NULL)
	{
		printf("ERRO: Nao ha memoria sufiente\n");
		exit(1);
	}

	//aux->nome = nome;
	//aux->idade = idade;
	//aux->cpf = cpf;
	//aux->sexo = sexo;
	//aux->curso = curso;
	//aux->contas = financa;
	//aux->pagou = pagou;

	return aux;
}

void libera_moradores(Morador** morador, int quantidade_moradores)
{
	for (int i = 0; i < quantidade_moradores; i++)
	{
		delete morador[i];
	}

	delete morador;
}

void set_nome_morador(Morador* morador, string nome)
{
	morador->nome = nome;
}

void set_idade(Morador* morador, int idade)
{
	morador->idade = idade;
}

void set_cpf(Morador* morador, long int cpf)
{
	morador->cpf = cpf;
}

void set_sexo(Morador* morador, char sexo)
{
	morador->sexo = sexo;
}

void set_curso(Morador* morador, string curso)
{
	morador->curso = curso;
}

void set_conta(Morador* morador, Financa* financa)
{
	morador->contas = financa;
}

void set_pagou_conta(Morador* morador, bool conta)
{
	morador->pagou = conta;
}


string get_nome_morador(Morador* morador)
{
	return string(morador->nome);
}

int get_idade(Morador* morador)
{
	return morador->idade;
}

long int get_cpf(Morador* morador)
{
	return morador->cpf;
}

char get_sexo(Morador* morador)
{
	return morador->sexo;
}

string get_curso(Morador* morador)
{
	return string(morador->curso);
}

bool get_pagou_conta(Morador* morador)
{
	return morador->pagou;
}

Financa* get_conta(Morador* morador)
{
	return morador->contas;
}

void ordenar_moradores(Morador** moradores, int quant_moradores)
{
    mergeSort(moradores, 0, quant_moradores - 1);
}

void merge(Morador** vetor, int comeco, int meio, int fim) {
	int com1 = comeco, com2 = meio + 1, comAux = 0, tam = fim - comeco + 1;
	Morador* vetAux;
	vetAux = (Morador*)malloc(tam * sizeof(Morador));

	while (com1 <= meio && com2 <= fim) {
		if (vetor[com1]->nome < vetor[com2]->nome) {
			vetAux[comAux] = *vetor[com1];
			com1++;
		}
		else {
			vetAux[comAux] = *vetor[com2];
			com2++;
		}
		comAux++;
	}

	while (com1 <= meio) {  //Caso ainda haja elementos na primeira metade
		vetAux[comAux] = *vetor[com1];
		comAux++;
		com1++;
	}

	while (com2 <= fim) {   //Caso ainda haja elementos na segunda metade
		vetAux[comAux] = *vetor[com2];
		comAux++;
		com2++;
	}

	for (comAux = comeco; comAux <= fim; comAux++) {    //Move os elementos de volta para o vetor original
		*vetor[comAux] = vetAux[comAux - comeco];
	}

	free(vetAux);
}

void mergeSort(Morador** vetor, int comeco, int fim) {
	if (comeco < fim) {
		int meio = (fim + comeco) / 2;

		mergeSort(vetor, comeco, meio);
		mergeSort(vetor, meio + 1, fim);
		merge(vetor, comeco, meio, fim);
	}
}

int busca_morador(Morador** moradores, int quant_moradores, string nome_morador)
{
	int aux = PesquisaBinaria(nome_morador, moradores, 0,quant_moradores);
    return aux;
}

int PesquisaBinaria (string  nome, Morador** v, int e, int d)
{
	int meio = (e + d)/2;
	if (v[meio]->nome == nome)
		return meio;
	if (e >= d)
		return -1; // não encontrado
	else
		if (v[meio]->nome < nome)
			return PesquisaBinaria(nome, v, meio+1,      d);
		else
			return PesquisaBinaria(nome, v,      e, meio-1);
}

void print_morador(Morador* morador)
{
	//system("clear");
	if(morador == NULL)
	{
		cout << "ERRO: Morador nao Encontrado" << endl;
	}
	else
	{
		cout << "\nNome: " << morador->nome
			<< "\nIdade: " << morador->idade
			<< "\nCPF: " << morador->cpf
			<< "\nSexo: " << morador->sexo
			<< "\nCurso: " << morador->curso << endl;
		cout << "Contas:\n";
		print_gastos(get_gastos(get_conta(morador)));
			cout << "\nPagou conta: " << morador->pagou
			<< endl;
	}
	
}