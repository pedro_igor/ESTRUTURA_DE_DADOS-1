#ifndef REPUBLICA_HPP
#define REPUBLICA_HPP

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include "morador.hpp"
#include "financas.hpp"
#include "evento.hpp"

using std::string;
using namespace std;

typedef struct republica Republica;

Republica** cria_Republicas(int quant_republica);

Republica* cria_Republica(string nome_rep, int capacidade, int quantidade_atual, string bairro, string tipo_genero, string tipo);

void libera_republicas(Republica** republica, int quantidade_republicas);

void set_nome_republica(Republica* republica, string nome_republica);

void set_capacidade(Republica* republica, int capacidade);//capacidade maxima da republica

void set_quantide(Republica* republica, int quantidade);//quantidade atual da republica

void set_bairro(Republica* republica, string bairro);

void set_tipo_genero(Republica* republica, string tipo_genero);// se a republica eh masculina, feminina ou mista

void set_tipo(Republica* republica, string tipo);// tipo da republica, se eh federal ou particular

void set_moradores(Republica* republica, Morador** moradores);

void set_morador(Republica* republica, Morador* morador, int indice);

//void set_gastos(Republica* republica, Gasto** contas);

//void set_eventos(Republica* republica, Evento** eventos);

//void set_quant_evento(Republica* republica, int quantidade);

void set_financas(Republica* republica, Financa* financa);

string get_nome_republica(Republica* republica);

int get_capacidade(Republica* republica);

int get_quantidade_atual(Republica* republica);

string get_bairro(Republica* republica);

string get_tipo_genero(Republica* republica);

string get_tipo(Republica* republica);

Morador** get_moradores(Republica* republica);

Morador* get_morador(Republica* republica, int indice);

//Gasto** get_gastos(Republica* republica);

//Evento** get_evento(Republica* republica);

Financa* get_financa(Republica* republica);

//int get_quant_evento(Republica* republica);

int busca_republica(Republica** republica, int quant_republica, string nome_republica);

void ordenar_republica(Republica** republicas, int quant_republica);

void mergeSort_republica(Republica** vetor, int comeco, int fim);

void merge_republica(Republica** vetor, int comeco, int meio, int fim);

int PesquisaBinaria_republica (string  nome, Republica** v, int e, int d);

void print_republica(Republica* republica);

#endif // !REPUBLICA_HPP
