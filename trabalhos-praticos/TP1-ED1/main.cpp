 // TP_1.cpp : Este arquivo contém a função 'main'. A execução do programa começa e termina aqui.
//
/**********************************************************************
**            UNIVERSIDADE FEDERAL DE OURO PRETO - UFOP              **
**         INSTITUTO DE CIENCIA EXATAS E BIOLÓGICAS - ICEB           **
**               DEPARTAMENTO DE COMPUTAÇÃO - DECOM                  **
**                 ESTRUTURA DA DADOS I - BCC202                     **
**                                                                   **
**PROFESSORA: AMANDA SÁVIO NASCIMENTO                                **
**ALUNOS: MARIA LAURA MOREIRA R                                      **
**        PEDRO IGOR DE SOUZA MALAQUIAS                              **
**********************************************************************/
#include <iostream>
#include <cstdio>
#include <string>
#include <cstring>

#include "menus.hpp"
#include "republica.hpp"
#include "morador.hpp"
#include "evento.hpp"
#include "gasto.hpp"
#include "data.hpp"

using namespace std;
using std::string;


int main()
{
	//system("clear");//limpa o terminal

	int quantidade_republicas;
	//vairiaveis ultilizado em republica
	Republica** republica;
	string nome_republica;//nome de uma republica
	int capacidade;//maximo de moradores a republica suporta
	int quantidade_atual;//quantidade atual de moradores de uma republica
	string bairro;//bairro onde a republica esta localizada
	string tipo_genero;//se a republica eh do tipo feminino, masculino, ou mista
	string tipo;//se a republica eh federal ou particular
	int cont_eventos;//quantidade de eventos
	Financa* financas_republica;// contas de uma republica

	//variaveis para morador
	Morador** morador;//vetor onde amarzena moradores em cada republica
	string nome_morador;//nome de morador
	int idade;//idade de morador
	long int cpf;//cpf do morador
	char sexo;//feminino ou masculino
	string curso;//curso do morador na universidade
	Financa* contas;
	bool pagou = false;

	//variaveis para evento
	Evento** evento;
	string nome_evento;
	double dinheiro;//valor arrecadado em um evento
	double gastos;//o que foi gasto em um evento
	double lucro;//o que obteve de lucro (gasto - dinheiro)

	//vairaveis para financas
	Gasto** gastos_republica;
	Evento** eventos;
	Gasto** gasto_morador;

	//variaveis para data
	//Data* date;
	int dia;
	int mes;
	int ano;

	//variaveis para gastos
	//Descricao descricao;
	string descricao;
	double valor;
	//Data** vencimento;

	//variaveis para recibo
	//Data* prazo;
	Data* data;
	//int status;

	//variaveis auxiliares
	int cont = 0;
	int aux_republica = 0;

	int indice = 0;
	int indice_2 = 0;
	//Republica* aux_republicaa;//quando for procurar republica

	int op;//variavel para o menu
	do
	{
		if (cont == 0)
		{
			op = 1;
		}
		else
		{
			menu_principal();
			cin >> op;
		}

		switch (op)
		{
			//inicio de manipulação de republica
			case 1:
				do
				{
					if (aux_republica == 0)
					{
						cout << "REPUBLICA" << endl;
						op = 1;
					}
					else
					{
						menu_republica();
						cin >> op;
					}

					switch (op)
					{
						case 1:
							cout << "\nCRIAR REPUBLICA" << endl;

							cout << "Qual eh a quantidade de republicas: ";
							cin >> quantidade_republicas;

							republica = cria_Republicas(quantidade_republicas); //aqui cria um conjunto de republicas

							for (int i = 0; i < quantidade_republicas; i++)
							{
								cin.ignore();
								cout << "\nQual eh o nome da republica " << i + 1 << ": ";
								//cin >> nome_republica;
								cin >> nome_republica;;

								cout << "\nQual eh a capacidade maxima da republica: ";
								cin >> capacidade;

								do
								{
									cin.ignore();
									cout << "\nQual eh a quantidade atual de moradores: ";
									cin >> quantidade_atual;

								}while(quantidade_atual > capacidade);

								//cin.ignore();
								cout << "\nQual eh o bairro onde esta localizada: ";
								cin >> bairro;

								//cin.ignore();
								cout << "\nFeminina, Masculina, ou Mista: ";
								cin >> tipo_genero;

								//cin.ignore();
								cout << "\nFederal ou Particular: ";
								cin >> tipo;

								gastos_republica = criar_gastos();

								for(int j = 0; j < 6; j++)
								{
									Gasto* aux = criar_gasto();

									//system("clear");
									cout << "\nDigite o nome da conta: ";
									cin >> descricao;
									set_descricao(aux, descricao);

									//getchar();
									cout <<"\nDigite o valor total da conta: ";
									cin >> valor;
									set_valor(aux, valor);
									
									//cout << " endl" << endl;
									//cin.ignore();
									cout << "\nDigite a data de vencimento (dd mm aa): ";
									cin >> dia >> mes >> ano;

									//cout << " endl11" << endl;
									
									data = cria_data(dia, mes, ano);
									set_vencimento(aux, data);

									//gastos_republica[j] = cria_gasto(descricao, valor, data);
									gastos_republica[j] = aux;
									//cout << get_gastos(gastos_republica[j]);
								}
								//morador = cria_moradores(capacidade);

								republica[i] = cria_Republica(nome_republica, capacidade, quantidade_atual, bairro, tipo_genero, tipo);
								financas_republica = cria_financa(gastos_republica);
								set_financas(republica[i], financas_republica);
								
								//cout << "\oi!!!";
							}

							//cout << "ok";
							ordenar_republica(republica, quantidade_republicas);
							//cout << "sei la" << endl;

							break;

						case 2:
							cout << "\nDELETAR REPUBLICAS" << endl;
							libera_republicas(republica, quantidade_republicas);
							cout << "Republicas deletadas com sucesso!" << endl;
							break;

						case 3:
							cout << "\nATUALIZAR REPUBLICA" << endl;

							cin.ignore();
							cout << "\nDigite o onme da republica que quer atualizar informacao: ";
							cin >> nome_republica;;

							
							indice = busca_republica(republica, quantidade_republicas, nome_republica);
							//indice = PesquisaBinaria_republica(nome_republica, republica, 0, quantidade_republicas);
							
							cout << "\nAtulatizar informacoes da republica " << get_nome_republica(republica[indice]);

							cout << "\nQual eh a capacidade maxima da republica: ";
							cin >> capacidade;

							set_capacidade(republica[indice], capacidade);

							cin.ignore();
							cout << "\nQual eh a quantidade atual de moradores: ";
							cin >> quantidade_atual;

							set_quantide(republica[indice], quantidade_atual);

							cin.ignore();
							cout << "\nQual eh o bairro onde esta localizada: ";
							cin >> bairro;

							set_bairro(republica[indice], bairro);

							//cin.ignore();
							cout << "\nFeminina, Masculina, ou Mista: ";
							cin >> tipo_genero;

							set_tipo_genero(republica[indice], tipo_genero);

							cin.ignore();
							cout << "\nFederal ou Particular: ";
							cin >> tipo;

							set_tipo(republica[indice], tipo);

							cout << "Republica " << get_nome_republica(republica[indice]) << " atualizada com sucesso" << endl;

							
							break;

						case 4:
							cout << "\nIMPRIMIR INFORMACOES DA REPUBLICA" << endl;

							cin.ignore();
							cout << "\nDigite o nome da republica que deseja exibir informacoes: ";
							//cin >> nome_republica;
							cin >> nome_republica;;

							indice = busca_republica(republica, quantidade_republicas, nome_republica);

							if(indice != -1)
							{
								print_republica(republica[indice]);
								cout << "Aparte enter para continuar" << endl;
								getchar();
							}
							else
							{
								cout << "Republica nao encontrada" << endl;
							}
							
							break;

						case -1:
							cout << "\nMENU ANTERIOR" << endl;
							break;

						default:
							cout << "ERRO: opcao invalida\n" << endl;
							break;
					}

					aux_republica++;
				} while (op != -1);

				break;//fim de manipulação de republica

			//Inicio de manipulação de moradores
			case 2:
				
				do 
				{
					op = 0;
					menu_morador();
					cin >> op;
					//iniciao de submenu de moradores
					switch (op)
					{
						case 1:
						{
							cout << "CRIAR MORADOR" << endl;

							cin.ignore();
							cout << "\nDigite o nome da republica que quer criar moradores: ";
							cin >> nome_republica;

							indice = busca_republica(republica, quantidade_republicas, nome_republica);

							if (indice != -1)
							{
								quantidade_atual = get_quantidade_atual(republica[indice]);
								morador = get_moradores(republica[indice]);

								for (int i = 0; i < get_quantidade_atual(republica[indice]); i++)
								{
									cin.ignore();
									cout << "\nQual eh o nome do morador" << i + 1 << ": ";
									cin >> nome_morador;

									cout << "\nQual eh a idade de " << nome_morador << " : ";
									cin >> idade;

									cout << "\nQual eh o CPF de " << nome_morador << " : ";
									cin >> cpf;

									if (get_tipo_genero(republica[indice]) == "Masculina" || get_tipo_genero(republica[indice]) == "masculina")
									{
										//cout << "Qual eh o sexo de (F/M) " << nome_morador << " : ";
										sexo = 'M';
									}
									else if (get_tipo_genero(republica[indice]) == "Feminina" || get_tipo_genero(republica[indice]) == "feminina")
									{
										sexo = 'F';
									}
									else // caso a republica seja mista
									{
										cout << "\nQual eh o sexo de (F/M) " << nome_morador << " : ";
										cin >> sexo;
									}

									cin.ignore();
									cout << "\nQual eh o curso de " << nome_morador << " : ";
									cin >> curso; 

									pagou = false;

									gasto_morador = get_gastos(get_financa(republica[indice]));

									for (int j = 0; j < 6; j++)
									{
										set_valor(gasto_morador[j], (get_valor(gasto_morador[j]) / get_quantidade_atual(republica[indice])));
									}

									contas = cria_financa(gasto_morador);

									//republica[indice] = cria_morador(nome_morador, idade, cpf, sexo, curso, contas, pagou);

									set_nome_morador(morador[i], nome_morador);
									set_idade(morador[i], idade);
									set_cpf(morador[i], cpf);
									set_sexo(morador[i], sexo);
									set_curso(morador[i], curso);
									set_conta(morador[i], contas);
									set_pagou_conta(morador[i], pagou);
								}

								//cout << "ola" << endl;
								ordenar_moradores(morador, get_quantidade_atual(republica[indice]));
							}
							else
							{
								cout << "ERRO: republica nao encontrada " << endl;
							}


							break;//aqui devia sair do case
						}//fim do case 1 do submenu-moradores

						case 2: //deleta todos os moradores
						{
							cout << "DELETAR TODOS OS MORADORES" << endl;

							cin.ignore();
							cout << "\nDigite o nome da republica que quer deletar moradores: ";
							cin >> nome_republica;

							indice = busca_republica(republica, quantidade_republicas, nome_republica);

							if (indice != -1)
							{
								libera_moradores(get_moradores(republica[indice]), get_capacidade(republica[indice]));
								cout << "Moradores liberado com sucesso" << endl;
							}
							else
							{
								cout << "Republica nao encontrada" << endl;
							}

							break;
						}
						case 3: //atualiza informaçoes do morador
						{
							cout << "ATUALIZAR INFOMACOES DE MORADOR" << endl;

							cin.ignore();
							cout << "\nDigite o nome da republica que quer atualizar os dados de moradore: ";
							cin >> nome_republica;
						

							indice = busca_republica(republica, quantidade_republicas, nome_republica);

							if (indice != -1)
							{
								cout << "Digite o nome do morador da republica :"<<endl;
								cin >> nome_morador;

								indice_2 = busca_morador(get_moradores(republica[indice]), get_quantidade_atual(republica[indice]), nome_morador);

								if (indice_2 != -1)
								{
									morador = get_moradores(republica[indice]);

									cout << "\nDigite o curso do morador: " << get_nome_morador(morador[indice_2]) << endl;
									cin >> curso;

									set_curso(morador[indice_2], curso);

									char decisao;
									cout << "\nMorador pagou contas (S/N): ";
									cin >> decisao;

									if (decisao == 's' || decisao == 'S')
									{
										pagou = true;
										set_pagou_conta(morador[indice_2], pagou);
									}
									else
									{
										//pagou = false;

									}

									cout << "Morador atualizado com sucesso\n";
								}
								else
								{
									cout << "Morador nao encontrado" << endl;
									break;
								}
							}
							else
							{
								cout << "REPUBLICA NAO ENCONTRADA" << endl;
							}

							break;
						}

						case 4:
						{

							cout << "\nIMPRIMIR INFORMACOES DE MORADOR" << endl;

							cin.ignore();
							cout << "\nDigite a republica onde esta localizado o morador: ";
							cin >> nome_republica;

							indice = busca_republica(republica, quantidade_republicas, nome_republica);

							if (indice != -1)
							{
								cin.ignore();
								cout << "\nDigite o nome do morador que deseja exibir informacoes: ";
								cin >> nome_morador;

								indice_2 = busca_morador(get_moradores(republica[indice]), get_quantidade_atual(republica[indice]), nome_morador);
								cout << "achou" << endl;

								if (indice_2 != -1)
								{
									morador = get_moradores(republica[indice]);
									print_morador(morador[indice_2]);
									cout << "Aparte enter para continuar" << endl;
									getchar();
								}
								else
								{
									cout << "Morador nao encontrado" << endl;
									break;
								}

							}
							else
							{
								cout << "Republica nao encontrada" << endl;
							}

							break;
						}
						case -1:
						{
							//menu anterior
							break;
						}
							
						default:
							cout << "ERRO: opcao invalida" << endl;
							break;
					}

				}while (op != -1);//fim de submenu moradores
			
				break;//fim de maipulação de moradores

			case 3://inicio de manipulacao de eventos

				do
				{
					menu_evento();
					cin >> op;

					switch (op)
					{
						case 1:
						{
							cout << "CRIAR EVENTOS" << endl;

							cin.ignore();
							cout << "Digite o nome da republica que deseja criar o evento: ";
							cin >> nome_republica;;

							indice = busca_republica(republica, quantidade_republicas, nome_republica);

							if (indice != -1)
							{
								cout << "Qual eh a quantidade de eventos: ";
								cin >> cont_eventos;

								evento = cria_eventos(cont_eventos);

								for (int i = 0; i < cont_eventos; i++)
								{
									cin.ignore();
									cout << "\nQual eh o nome do evento: ";
									cin >> nome_evento;

									cout << "\nQual foi o valor arecadado: ";
									cin >> dinheiro;

									cout << "\nQual foi o valor gasto: ";
									cin >> gastos;

									lucro = dinheiro - gastos;
									//cout << "passou 2";
									//lucro = 0;
									//cout << "passou 1";
									evento[i] = cria_evento(nome_evento, dinheiro, gastos, lucro);
									//out << "passou 2";
								}

								quantidade_eventos(get_financa(republica[indice]), cont_eventos);
								set_eventos(get_financa(republica[indice]), evento);

							}
							else
							{
								cout << "ERRO: Republica nao encontrada" << endl;
							}

							//cria_evento
							break;
						}

						case 2:
						{
							cout << "Deletar eventos" << endl;

							cin.ignore();
							cout << "Digite o nome da republica: ";
							cin >> nome_republica;

							indice = busca_republica(republica, quantidade_republicas, nome_republica);

							if (indice == -1)
							{
								cont_eventos = get_quant_eventos(get_financa(republica[indice]));
								libera_eventos(get_eventos(get_financa(republica[indice])), cont_eventos);
								cout << "Eventos liberados com sucesso" << endl;
							}
							else
							{
								cout << "ERRO: republica nao encontrada" << endl;
							}


							break;
						}

						case 3:
						{
							cout << "Atulaizar eventos " << endl;

							cin.ignore();
							cout << "Digite o nome da republica: ";
							cin >> nome_republica;

							indice = busca_republica(republica, quantidade_republicas, nome_republica);

							if (indice != -1)
							{
								cin.ignore();
								cout << "Digite o nome do evento que quer atualizar da republica " << get_nome_republica(republica[indice]) << ": ";
								cin >> nome_evento;

								cont_eventos = get_quant_eventos(get_financa(republica[indice]));
								indice_2 = busca_evento(get_eventos(get_financa(republica[indice])), cont_eventos, nome_evento);

								if (indice_2 != -1)
								{
									eventos = get_eventos(get_financa(republica[indice]));

									cin.ignore();
									cout << "Nome do evento: ";
									cin >> nome_evento;

									cout << "Total arregadado: ";
									cin >> dinheiro;

									cout << "Gasto total: ";
									cin >> gastos;

									lucro = gastos - dinheiro;//calcula o lucro

									//atualiza os dados
									set_nome_evento(evento[indice_2], nome_evento);
									set_dinheiro(evento[indice_2], dinheiro);
									set_gastos(eventos[indice_2], gastos);
									set_lucro(eventos[indice_2], lucro);
								}
								else
								{
									cout << "ERRO: evento nao encontrado" << endl;
								}

							}
							else
							{
								cout << "ERRO: Republica nao encontrada" << endl;
							}

							break;
						}

						case 4:
						{
							cout << "Exibir informacoes do evento" << endl;

							cin.ignore();
							cout << "Digite o nome da republica: ";
							//cin >> nome_republica;
							cin >> nome_republica;;

							indice = busca_republica(republica, quantidade_republicas, nome_republica);

							if (indice != -1)
							{
								cont_eventos = get_quant_eventos(get_financa(republica[indice]));
								evento = get_eventos(get_financa(republica[indice]));

								print_evento(evento, cont_eventos);

								//cout << "Aperte enter para continuar" << endl;
								//getchar();
							}
							else
							{
								cout << "ERRO: republica nao encontrada " << endl;
							}
							break;
						}
						case -1:
							break;
						default:
							cout << "ERRO: opcao nao encontrada" << endl;

					}
				} while (op != -1);

				break;// fim de manipulacao de eventos

			case 0:
				cout << "\nVOCE SAIU DO SISTEMA" << endl;
				break;
			
			default:
				cout << "\nERRO: Opcao invalida" << endl;
				break;
		}

		cont++;//numeros de interaçoes do menu principal
	}while(op != 0);//fim do menu principal

	return 0;
}//fim do main
